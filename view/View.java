package com.koval.view;

import com.koval.controller.*;
import com.koval.exceptions.*;
import com.koval.model.Remedy;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

/**
 * View class that interacts with user with the help of controller
 */
public class View {

    private Controller controller;
    private Scanner input;
    private Map<String,String> commands;
    private Map<String, Executable> methods;
    private List<Remedy>bucket;
    private List<Remedy> remedies;

    /**
     * Main constructor of the class
     */
    public View(){
        controller = new ControllerImplemented();
        input = new Scanner(System.in);
        remedies = controller.getRemedies();
        bucket = controller.getBucket();

        commands = new HashMap<>();
        commands.put("1", "1 - Show the list of the goods.");
        commands.put("2", "2 - Total amount of goods and brands.");
        commands.put("3", "3 - Add chosen product to bucket.");
        commands.put("4", "4 - Delete chosen product from the bucket.");
        commands.put("5", "5 - Show customer`s bucket.");
        commands.put("6", "6 - Save customer`s bucket to file");
        commands.put("Exit", "Press Q to exit from the menu");

        methods = new HashMap<>();
        methods.put("1", this::firstMethod);
        methods.put("2", this::secondMethod);
        methods.put("3", this::thirdMethod);
        methods.put("4", this::fourthMethod);
        methods.put("5", this::fifthMethod);
        methods.put("6", this::sixthMethod);
    }

        /**
        * Method that outputs the list of all the products.
        */
        private void firstMethod(){

        for (Remedy tmp: remedies) {
            System.out.print(tmp.toString());
        }

        System.out.println("\n");
        }

        /**
        * Method that outputs number of different product brands and amount of all the products.
        */
        private void secondMethod(){
        System.out.println(controller.getStatistics());
        System.out.println("\n");
        }

        /**
        * Method that enables user to add a chosen product to the user`s bucket.
        */
        private void thirdMethod() {
            System.out.println("Input the id of the product you want to add");
            int id = input.nextInt();
            boolean check = true;

            try {
                if(id < 0)
                    throw new ProductIdException("Wrong id input!");
                for (Remedy rm : remedies) {

                    if (rm.getId() == id) {
                        controller.addToBucket(rm);
                        check = false;
                        break;
                    }

                }

                if(check)
                    throw new ProductIdException("No such product in the list");

                System.out.println("Done!");

            }catch (ProductIdException exc){
                exc.printStackTrace();
            }
        }

        /**
        * Method that enables user to delete a chosen from user`s bucket.
        */
        private void fourthMethod() {

            try {

                if (bucket.isEmpty())
                    throw new EmptyBucketException("Cannot delete element from an empty bucket");

                System.out.println("Input the id of the product you want to delete");
                int id = input.nextInt();

                try {

                    if (id < 0)
                        throw new ProductIdException("Wrong id input!");

                    for (Remedy rm : remedies) {

                        if (rm.getId() == id) {

                            if (bucket.contains(rm)) {
                                controller.deleteFromBucket(rm);
                                break;

                            } else {
                                throw new EmptyBucketException("No such product id in the bucket");
                            }
                        }
                    }

                    System.out.println("Done!");

                } catch (ProductIdException | EmptyBucketException exc) {
                    exc.printStackTrace();
                }

            } catch (EmptyBucketException exc){
                exc.printStackTrace();
            }
        }

        /**
        * Method that shows off user`s bucket taking into account all the previous changes.
        */
        private void fifthMethod(){

            try {
                if(bucket.isEmpty())
                    throw new EmptyBucketException("The bucket is empty");
                else
                    controller.showBucket();

            }catch (EmptyBucketException exc){
                exc.printStackTrace();
            }
        }

    /**
     * Method that writes user`s bucket to the file taking into account all the previous changes.
     */
    private void sixthMethod(){
            try {

                if(bucket.isEmpty())throw new EmptyBucketException("Your bucket is empty");

                else controller.WriteToFile();

            } catch (EmptyBucketException exc){
                exc.printStackTrace();
            }
        }

        /**
        * Method that outputs products` menu.
        */
        private void showMenu(){
            System.out.println("~~~Goods` menu~~~");

            for (String str: commands.values()) {
                System.out.println(str);
            }

            System.out.println("~~~Goods` menu~~~\n");
        }

        /**
        * Interactive method that outputs products` menu and interact with user while program is being run.
        */
        public void interact(){
            String key;

            do {
                showMenu();
                System.out.println("Input an option (Press Enter button first if there is chosen option " +
                        "in the menu but you received exception)");
                input.nextLine();
                key = input.nextLine().toUpperCase();

                try {

                    if (methods.containsKey(key)) {
                        methods.get(key).execute();
                    } else {
                        throw new MenuOptionException("No such option in the menu");
                    }

                }catch (MenuOptionException exc){
                    exc.printStackTrace();
                }

            }while (!key.equals("Q"));

            System.out.println("Thanks for using this program. Bye!");
        }
    }