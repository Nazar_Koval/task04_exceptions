package com.koval.view;

/**
 * Functional interface for printing menu options
 */
@FunctionalInterface
public interface Executable {
    /**
     * Method to execute options from user`s menu.
     */
    void execute();
}
