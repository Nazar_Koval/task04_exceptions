package com.koval.controller;

import com.koval.model.*;
import java.util.List;

/**
 * Main controller to interact with user
 */
public interface Controller {

    /**
     *  Method that returns list of products received from the model
     * @return products` list
     */
    List<Remedy>getRemedies();

    /**
     * Method that returns the main statistics received from the model
     * @return total amount of products and all the different brand names
     */
    String getStatistics();

    /**
     * Method that creates a bucket to store chosen products
     * @return user`s bucket
     */
    List<Remedy> getBucket();

    /**
     * Method that enables user to add a product to the bucket
     * @param remedy Chosen product
     */
    void addToBucket(final Remedy remedy);

    /**
     * Method that enables user to delete a product from the bucket
     * @param remedy Chosen product
     */
    void deleteFromBucket(final Remedy remedy);

    /**
     * Method that shows of the bucket to the user
     */
    void showBucket();

    void WriteToFile();
}
