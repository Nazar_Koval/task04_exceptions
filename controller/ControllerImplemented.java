package com.koval.controller;

import com.koval.model.*;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * Realization of the controller.
 *  Description of all the methods here is inside Controller interface.
 */
public class ControllerImplemented implements Controller{

    public static final String FILEPATH = "E:\\task_03\\User`s bucket.txt";
    private Model model;
    private List<Remedy> bucket;

    public ControllerImplemented(){
        model = new Logic();
        bucket = new LinkedList<>();
    }

    @Override
    public List<Remedy> getRemedies() {
        return model.getRemediesList();
    }

    @Override
    public String getStatistics() {
        return model.getStatistics();
    }

    @Override
    public List<Remedy> getBucket(){
        return bucket;
    }

    @Override
    public void addToBucket(final Remedy remedy){
        bucket.add(remedy);
    }

    @Override
    public void deleteFromBucket(final Remedy remedy){
        bucket.remove(remedy);
    }

    @Override
    public void showBucket(){
        for (Remedy element: bucket) {
            System.out.println(element.toString());
        }
    }

    @Override
    public void WriteToFile(){
        try(FileWriter fileWriter = new FileWriter(FILEPATH)){

            for (Remedy element:bucket) {
                fileWriter.write("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
                fileWriter.write("Product name: " + element.getName() + "\n");
                fileWriter.write("Product price: " + element.getPrice() + "\n");
                fileWriter.write("Product amount: " + element.getAmount() + "\n");
                fileWriter.write("Product belonging:" + element.getBelonging() + "\n");
                fileWriter.write("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n");
            }

        }catch (IOException ex){
            System.err.println("An IOException was caught: " + ex.getMessage());
            ex.printStackTrace();
        }
    }
}