package com.koval;

import com.koval.view.*;
/**
 * Main class to execute the program
 */
public class Main {

    public static void main(String[] args)
    {
        new View().interact();
    }

}