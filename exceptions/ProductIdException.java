package com.koval.exceptions;

/**
 * Custom exception to check whether product id exists.
 */
public class ProductIdException  extends RuntimeException{
    private String message;

    /**
     * Default JavaBeans constructor
     */
    ProductIdException(){}

    /**
     * Constructor with an exception`s log
     * @param message message received after throwing exception
     */
    public ProductIdException(String message){
        this.message = message;
    }

    /**
     * Getter method
     * @return log message
     */
    public String getMessage(){
        return this.message;
    }
}
