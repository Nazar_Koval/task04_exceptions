package com.koval.exceptions;

/**
 * Custom exception for checking whether user`s bucket is empty.
 */
public class EmptyBucketException extends RuntimeException {
    private String message;

    /**
     * Default JavaBeans constructor
     */
    EmptyBucketException(){}

    /**
     * Constructor with an exception`s log
     * @param message message received after throwing exception
     */
    public EmptyBucketException(String message){
        this.message = message;
    }

    /**
     * Getter method
     * @return log message
     */
    public String getMessage(){
        return this.message;
    }
}
