package com.koval.exceptions;

/**
 * Custom exception to check whether there is a particular option in user`s menu.
 */
public class MenuOptionException extends RuntimeException {
    private String message;

    /**
     * Default JavaBeans constructor
     */
    MenuOptionException(){}

    /**
     * Constructor with an exception`s log
     * @param message message received after throwing exception
     */
    public MenuOptionException(String message){
        this.message = message;
    }

    /**
     * Getter method
     * @return log message
     */
    public String getMessage(){
        return this.message;
    }
}
