package com.koval.model;

import java.util.List;

/**
 * Interface that will represent main model to the controller
 */
public interface Model {
    /**
     *  Method that returns list with all the products
     * @return products` list
     */
    List<Remedy> getRemediesList();

    /**
     * Method that returns main statistics of the products` list
     * @return total amount of goods and all the different names
     */
    String getStatistics();
}
