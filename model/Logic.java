package com.koval.model;

import java.util.List;

/**
 * Realization of the Model interface
 * Description of all the methods here is inside Model interface
 */
public class Logic implements Model {

    private Remedy remedy;

    public Logic(){
        remedy = new Remedy();
    }

    @Override
    public List<Remedy> getRemediesList() {
        return remedy.getRemedies();
    }

    @Override
    public String getStatistics() {
        return "Number of all brands available: " + remedy.getAllBrands() + " c.u." + "\n" +
                "Total amount of goods: " + remedy.getTotalAmount() + " c.u.";
    }

}
